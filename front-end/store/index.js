export const state = () =>({
    auth:false
})

export const actions = {
    setAuth(status){
        commit("ChangeAuth",status)
    }
}

export const mutations = {
    ChangeAuth(state,status){
        state.auth = status
    }
}

