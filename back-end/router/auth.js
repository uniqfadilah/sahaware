const jwt = require('jsonwebtoken')

module.exports = function (request,response,next){
    const token = request.cookies['auth-token'];
    if(!token) return response.status(401)
    try {
        const verified = jwt.verify(token,process.env.TOKEN);
        request.user = verified;
        next()
    }catch(error){
        response.status(401)
    }
}