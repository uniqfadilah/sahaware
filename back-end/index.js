const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors')
const cookieParser = require('cookie-parser');
const Router = require('./router/article')
const multer = require('multer');
const path = require('path');
dotenv.config();

const fileStorage = multer.diskStorage({
  destination: (request, file , cb ) => {
    cb(null,'assets');
  },
  filename: (request,file,cb) => {
    cb(null,new Date().getTime()+file.originalname)
  }

})

const fileFilter = (request,file,cb) => {  
  if(file.mimetype === 'image/png' ||file.mimetype === 'image/jpg' ||file.mimetype === 'image/jpeg'){
    cb(null,true)
  }else{
    cb(null,false)
  }
}

mongoose.connect(process.env.DB_CONNECT,()=>console.log('connect'))
const corsConfig = {
  credentials: true,
  origin: true,
};



app.use(cors(corsConfig));
app.use(cookieParser());
app.use(express.json())
app.use('/assets',express.static(path.join(__dirname,'assets')))
app.use(express.urlencoded({extended:true}))
app.use(multer({storage:fileStorage, fileFilter:fileFilter}).single('thumb'))
app.use(Router)
app.get('/', function (req, res) {
    res.send('hello world')
  })
app.listen(process.env.PORT || 3000, function(){
    console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
  });
