const mongoose = require('mongoose');

const {Schema} =  mongoose;

const UserShema = new Schema ({
    fullname : String,
    email : String,
    password : String,
},{timestamps : true})

const User = mongoose.model('User',UserShema) 

module.exports = User;