const mongoose = require('mongoose');

const {Schema} =  mongoose;

const ArticleShema = new Schema ({
    title : String,
    shortDesc : String,
    thumb : String,
    value : String,
    categories :String
},{timestamps : true})

const Article = mongoose.model('Article',ArticleShema) 

module.exports = Article;