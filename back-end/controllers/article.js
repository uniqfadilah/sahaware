const Article = require('../model/article')
module.exports = {
    index : function(request,response){
        Article.find(
        function(error,result){
            !error?response.send(result):response.status(500)}
        ).sort({_id:-1});
    },
    show : function(request, response){
        const params = request.params.id
        Article.findOne({ _id: params }, function (error,result) {
            !error?response.send(result):response.status(500)
        });
    },
    post : function(request,response){
        var filepath = null
        if(request.file){
            filepath =  request.file.path
        }
        Article.create({
            title : request.body.title,
            shortDesc : request.body.shortDesc,
            thumb : filepath,
            value : request.body.value,
            categories : request.body.categories
        },
        function(error,result){
            !error?response.json(result):response.json(error)}
        )
    },
    categories: function(request,response){
        const params = request.params.categories
        Article.find(
            function(error,result){
                !error?response.send(result):response.send(500)}
            ).where('categories',params).sort({_id:-1})           ;
        
    }
}