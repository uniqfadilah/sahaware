const User = require('../model/user')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
module.exports = {
    login : async(request, response)=>{
        const user = await User.findOne({email:request.body.email});
        if(!user) return response.status(401)
        .json({
            status:false,
            message:'Email not registered'
        })
        const checpassword = await bcrypt.compare(request.body.password,user.password);
        if(!checpassword) return response.status(401).json({
            status:false,
            message:'Password invalid'
        })
        const token = jwt.sign({_id:user._id, name:user.fullname},process.env.TOKEN)
        response.cookie("auth-token",token,{
            maxAge: 1000*60*60*1,
            httpOnly: true, 
            secure: true,
            sameSite: 'none'
        })
        response.send({
            status:true
        })
    },
    logout: (request, response) =>{
        response.cookie("auth-token",'',{
            maxAge :0,
            httpOnly: true, 
            secure: true,
            sameSite: 'none'})
        response.status(200).send({
            message:'Berhasil LogOut'
        })
    },
    user: (request,response) => {
        const data = request.user
        response.status(200).send({
            data:data
        });
          
    },
    post : async (request,response) => {
        const user = await User.findOne({email:request.body.email});
        const salt = await bcrypt.genSalt(10);
        const password = await bcrypt.hash(request.body.password,salt)
        if(user) return response.status(400).send('email terdaftar')
         const data = await User.create({
            fullname : request.body.fullname,
            email : request.body.email,
            password : password,
        })
        return response.status(200).json(data)

   
    },
}